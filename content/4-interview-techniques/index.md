# Interview Techniques

![](images/1.png)

#### What are some basic techniques for getting the most out of an interview?

*Time to complete: 5-10 minutes*

---

In this lesson you will learn:

* How to prepare for an interview
* How to get the best story out of your sources
* Some basic questions to get you started interviewing

---

Interviewing well is a learned skill. In the last section you learned about identifying sources and the different kinds of information they are best qualified to provide. In this section you will learn how to approach your subject or source to get the information you need on the record.

Some interviews can be prearranged. Some are quick, unplanned and done at the scene. Good reporters often have established contacts — experts such as informational sources or authoritative voices — they can call for an interview on short notice in the event of breaking news or a tight deadline.

The type of story you are doing and the circumstances of the interview will determine the techniques you use and your ability to apply the principles introduced here. Recording interviews has special requirements; you need to be sure you have useable recorded material. This will be covered in the audio and video modules.

---

![](images/2.png)

The type of sources you are interviewing and the role they will play in your coverage should determine the kinds of questions you ask. The amount of time you have will also affect how you conduct your interview.

---

## Preparing for an interview

### Background research

Do some research before the interview when possible. You will ask better, more relevant questions. It helps to know a bit about the person you are interviewing.

### Prepare some introductory questions

It is always a good idea to have some basic questions prepared. It saves time and keeps the interview focused.

---

### Example of well-prepared notes for an interview

### Intro or warm-up questions

Ask these early to make the subject comfortable; they are easy and impersonal questions.

* What is your full name (and title) as you would like it for the article? (Be sure to confirm the correct spelling of names — even ones that seem obvious.)
* Where did you study?
* What are the materials you use and why do you use them?

### Main interview questions

This where you collect the most interesting and useful information.

* How do you describe the purpose of your work to others?
* What are some of the influences or inspirations for your artwork?
* How did the revolution influence the themes and subjects that your artwork addresses?
* What role do artists play in your community?

### Closing questions

This is often where you ask the most difficult or revealing questions, along with the final question below.

* Your critics say you are little more than a street graffiti artist or simple illustrator. How do you respond to these criticisms?
* Is there anything else you would like to add or that you think is important for our audience to know?

>play video >Example of a beginning of an interview

---

## Have a one-sentence explanation of your story for your sources

You need to make it clear to your source what your story is about. This will help your source stay focused and give you relevant answers.

---

## Conducting an interview

### Identify yourself as a journalist

In many situations this will be obvious, but not in all situations. This is especially important online. It is ethically and professionally very important to do this, though there are some exceptions. (For more on this see section 8 on professional standards and ethics).

Let the subject know if you or another journalist will be using a camera to record photos or video — the subject should not be caught surprised or unprepared. In some places, explicit permission is needed to record audio or video. Know the laws where you are reporting. If permission is needed, ask the source first before recording, then ask again after recording so you have a record of it.

### Start with basic questions

Depending on the amount of time you have, it is important to start with basic, safe questions. Help people get comfortable before you begin asking more emotionally difficult or challenging questions. It may help to give people some information about yourself.

### Control the interview

Sometimes people have expectations for your interview that are different than your own. Politicians frequently try to turn an interview to subjects important to them. Private citizens unfamiliar with the interview process often talk about things irrelevant to the topic. You need to be polite but firm in controlling the interview. Monologues are never as informative as interviews consisting of questions and short, relevant answers.

### Do not rush the answer

While it is important to control the pace of the interview, it is important to let people complete a thought. People will sometimes try to fill an awkward silence by saying more — sometimes more than they should!

### Probe and clarify

Sometimes sources are not good at answering questions clearly. Other times they are intentionally trying to avoid answering a question. In either case, you can ask your question a different way. Sometimes it helps to restate the answer to the source: “so, you are saying that…” as a way to confirm an answer. With politicians it may help to ask a more general version of a question if they refuse to answer it specifically.

> play video > Example of how to control an interview, probe and clarify

### Ask follow-up questions

Don’t be so tied to your prepared questions and note-taking/recording that you forget to ask follow-up questions. Sometimes the most valuable information comes from questions you didn’t plan to ask.

### Save sensitive questions for the end

As a reporter you need to be tough. You have to ask the difficult questions and get answers. A good strategy for this is to save the most challenging questions for last. This way you still have material if the subject is angered by your tougher questions.

> play video >Example of asking a sensitive question and ending an interview

---

## Ending the interview

It is always a good idea to end the interview with two questions:

* *“Is there anything else you would like to say?”*

This gives your source the opportunity to give you information important to him/her or to answer questions you did not think to ask.

* *“Can I contact you if I have any more questions?”*

This will allow you to follow up in the event you or an editor need to clarify something or if there is a development in the story for which you need a response.

---

## THINGS TO REMEMBER

* Be prepared for your interview. Do some research. Have some questions ready.
* The most common challenges to a good interview are time and reluctant or difficult sources.
* Prioritize your questions and identify the single, most essential bit of information you need from a subject.
* Try to make your subject comfortable.
* If the source is difficult because s/he is uncomfortable or because there is something s/he does not want to tell you, an interview can sometimes become a negotiation.
* As a reporter you have to be tough but fair.
* Sometimes sources recognize that a “no comment” response or vague response to a question will make them look worse in the press than a straight answer. You may choose to point this out.
Your story will be more interesting if your interviews include people with opposing points of view.

---

## QUIZ

1. **What are some things you might do if you are interviewing someone reluctant to talk?**

* Move to your most essential questions more quickly/
* Ask why the subject is reluctant, and see if a solution can be negotiated.
* With a public figure, you might point out that a “no comment” will make him look bad.
* All of the above

2. **True or false: If a subject is reluctant to answer an important question, rather than restate the question differently or try to push the issue by probing, you should drop it.**

* True
* False

3. **Which of these things should you NEVER do in an interview:**

* Let the conversation fall silent for an awkward period of time
* Explain what the story is about
* At the end of the interview, ask the source if he or she has anything to add
* None of the above

---
