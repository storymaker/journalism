# Evolution of a Story

![](images/1.png)

#### How do I adapt my coverage as the story changes?

*Time to complete: 10-15 minutes*

---

In this lesson you will learn:

* How news organization tell stories over time, called the news cycle
* What different story types are called
* Techniques for keeping your coverage fresh as a story evolves

---

Some stories are like bright flares. They burn intensely but shortly. Other stories build slowly, growing in intensity as they develop. Other stories are short-lived but illustrate ongoing social issues that warrant continued coverage. All stories evolve over time. This is called the news cycle. Some stories have a very long cycle. For others the cycle can be short.

---

![](images/2.png)

News organizations think in terms of distinct categories for coverage:

* Highlights/breaking news stories
* Event stories
* Issue stories
* Profile stories

These categories help journalists plan their coverage as time goes on. They help journalists maintain the attention of readers and viewers by providing variety in their coverage throughout the cycle. Understanding this progression and what is expected of you will make you a better journalist.

---

## The highlights/breaking news story

From fires to demonstrations to plane crashes, breaking news stories are unplanned and require a fast response. Breaking news stories are often succinct dispatches that provide little more than basic information: who, what, where, when, why, and how. Think in terms of reporting the basic facts and interviewing sources in your coverage that get to the key information immediately. Breaking news stories are sometimes referred to as first-day stories if there will be follow-up coverage as the event unfolds. In a social environment, breaking news coverage may be little more than a short post or a photo.

> play video > Example of a breaking news story

> play video > Example of a breaking news story

> play video > Example of a following story

---

## The event story

This may be a press conference, planned demonstration, opening of a new hospital, or an election. Some event stories follow breaking news. One key difference from breaking stories is that journalists have the opportunity to do pre-reporting for event stories. This allows you to present more background information in your story or as links. It might also allow you time to talk to more people, perhaps including an expert voice or someone with an opposing viewpoint. Ask yourself what information and sources will help the reader better understand the issue. The medium you use can be determined by the story. For example, if the story has strong visual elements, video may be the best tool.

> play video > Example of an event story

---

## The issue story
Issue stories are about broader trends rather than specific events or individual people. They can be about important topics such as a water shortage or more light-hearted topics such as a funny fashion trend. Issue stories favor a more artful presentation than news stories. Issue stories often include descriptions of how things look or sound, or how people react.

> play video >Example of an issue story

---

## The profile story
A profile is a story about a single person or character. He or she could be a politician or an athlete or a celebrity. Profile stories give us a window into the world of that person. Like issue stories, profiles often contain description, as well as an interview with the subject and, often, interviews with friends, family members and associates.

> play video > Example of a profile story

---

## Coverage tips

Journalists and news organizations can present a compelling variety of coverage over time that encourages public understanding and dialogue about a topic. Here are some tips for producing ongoing coverage that evolves with the story:

* What’s new and what’s now: News updates need to be short and informative. Get to the point and get to it quickly. Your post should include the basic facts and most current information.
* Show and tell: While some stories are tough to visualize, others may lend themselves to photos, video, or audio. Choose the right medium or combination of media to tell your story. Do not forget to keyword and tag multimedia content.
* Dateline: Be sure to present exactly where and when the post and story covered were produced.
* Links to more information: This can be a Twitter feed or a YouTube video or a link to a previous posts or links to coverage from other news outlets. Consider using hashtags if they are relevant to your platform.
* Cite your sources: Let viewers know where your information is coming from — eyewitness accounts, an official spokesperson, your own observation, or other news sources. Transparency is key.
* Cultivate contribution: Encourage people to contribute to your reporting. Make it easy for them to comment. Promote more discussion with the goal of better information for all.
* Present your information in a logical order for the audience. Analysis about the causes of a power outage in the capitol should not precede breaking news coverage of its immediate impact on the public.
* As the storyline evolves, it is important to continue to make basic, background, and contextual information available. New audience members will begin consuming your coverage during different times; make sure the story always makes sense to audiences seeing the coverage for the first time.

---

## THINGS TO REMEMBER

* Identify what kind of story you are doing: breaking news, event, issue, or profile.
* Adjust your coverage throughout the story’s cycle.
* Always start your report with the newest information.
* Make your sources clear.
* In a social-first environment, it is important to encourage contribution from your audience.

---

## QUIZ

1. **Which of the following is a characteristic of a good news story? (There may be more than one answer.)**

* The journalist’s opinion is clear.
* The presentation of information shows only one point of view or one side of the story.
* While the presentation may present a point of view, it makes a fact-based argument for that point of view.
* The reader does not realize that it is an opinion piece rather than news.


2. **True or false: As a story moves from first-day coverage to more in-depth coverage, it is important to provide contextual information in your presentation that reminds your audience of the basic facts of the issue.**

* True
* False

3. **The categories of stories presented here are important because:**

* They are a roadmap for how journalists can present more information as a story evolves over time.
* The provide variety, which helps keep the audience interested in the topic over time.
* Clear categories give reporters and news organization a way to talk about stories and plan long-term coverage.
* All of the above

---
